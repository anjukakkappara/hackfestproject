﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABNAMRODigitalAssistant.StateInfoModels
{
    public enum BankAccountType { Savings, Current, Credit }

    public class BankAccount
    {
        public BankAccountType Type { get; set; }

        public double Balance { get; set; }

        public Dictionary<string,string> Address { get; set; }

        public Dictionary<string,string> DebitCardDetails { get; set; }

        public string LastTransaction { get; set; }

    }
}
