﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABNAMRODigitalAssistant.StateInfoModels
{
    public class UserProfile
    {
        public string Name { get; set; }

        public BankAccount Account { get; set; }

        public string ContactNo { get; set; }

    }
}
