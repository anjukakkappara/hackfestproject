﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ABNAMRODigitalAssistant.StateInfoModels;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace ABNAMRODigitalAssistant
{
    /// <summary>
    /// Represents a bot that processes incoming activities.
    /// For each user interaction, an instance of this class is created and the OnTurnAsync method is called.
    /// This is a Transient lifetime service.  Transient lifetime services are created
    /// each time they're requested. For each Activity received, a new instance of this
    /// class is created. Objects that are expensive to construct, or have a lifetime
    /// beyond the single turn, should be carefully managed.
    /// For example, the <see cref="MemoryStorage"/> object and associated
    /// <see cref="IStatePropertyAccessor{T}"/> object are created with a singleton lifetime.
    /// </summary>
    /// <seealso cref="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1"/>
    public class ABNAMRODigitalAssistantBot : IBot
    {
        private readonly ABNAMRODigitalAssistantServices _services;
        private readonly ABNAMRODigitalAssistantAccessors _accessors;

        private DialogSet _dialogs;

        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="conversationState">The managed conversation state.</param>
        /// <param name="loggerFactory">A <see cref="ILoggerFactory"/> that is hooked to the Azure App Service provider.</param>
        /// <seealso cref="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-2.1#windows-eventlog-provider"/>
        public ABNAMRODigitalAssistantBot(ConversationState conversationState, UserState userState, ABNAMRODigitalAssistantServices services, ILoggerFactory loggerFactory)
        {
            if (conversationState == null)
            {
                throw new System.ArgumentNullException(nameof(conversationState));
            }

            if (loggerFactory == null)
            {
                throw new System.ArgumentNullException(nameof(loggerFactory));
            }

            _accessors = new ABNAMRODigitalAssistantAccessors(conversationState, userState)
            {
                CounterState = conversationState.CreateProperty<CounterState>(ABNAMRODigitalAssistantAccessors.CounterStateName),
                UserProfileAccessor = userState.CreateProperty<StateInfoModels.UserProfile>(ABNAMRODigitalAssistantAccessors.UserProfileName),
                ConversationDataAccessor = conversationState.CreateProperty<StateInfoModels.ConversationData>(ABNAMRODigitalAssistantAccessors.ConversationDataName),
                ConversationDialogState = conversationState.CreateProperty<DialogState>("DialogState")
            };



            _services = services ?? throw new System.ArgumentNullException(nameof(services));
            if (!_services.LuisServices.ContainsKey("ABNAMRO"))
            {
                throw new System.ArgumentException($"Invalid configuration. Please check your '.bot' file for a LUIS service named 'ABNAMRO'.");
            }

            _dialogs = new DialogSet(_accessors.ConversationDialogState);

            var waterfallSteps = new WaterfallStep[]
            {
                TypeStepAsync,
                PostalCodeStepAsync,
                PostalCodeConfirmStepAsync,
                HouseNumberStepAsync,
                HouseNumberConfirmStepAsync,
                StreetNameStepAsync,
                StreetNameConfirmStepAsync,
                DetailsSummaryConfirmStepAsync,
                SummaryAsync

                //TownStepAsync,
                //TownConfirmStepAsync,
                //SummaryStepAsync,
            };

            _dialogs.Add(new WaterfallDialog("details", waterfallSteps));
            _dialogs.Add(new TextPrompt("text"));
            _dialogs.Add(new NumberPrompt<int>("age"));
            _dialogs.Add(new ConfirmPrompt("confirm"));
            _dialogs.Add(new ChoicePrompt("choice"));


            _logger = loggerFactory.CreateLogger<ABNAMRODigitalAssistantBot>();
            _logger.LogTrace("Turn start.");
        }

        /// <summary>
        /// Every conversation turn for our Echo Bot will call this method.
        /// There are no dialogs used, since it's "single turn" processing, meaning a single
        /// request and response.
        /// </summary>
        /// <param name="turnContext">A <see cref="ITurnContext"/> containing all the data needed
        /// for processing this conversation turn. </param>
        /// <param name="cancellationToken">(Optional) A <see cref="CancellationToken"/> that can be used by other objects
        /// or threads to receive notice of cancellation.</param>
        /// <returns>A <see cref="Task"/> that represents the work queued to execute.</returns>
        /// <seealso cref="BotStateSet"/>
        /// <seealso cref="ConversationState"/>
        /// <seealso cref="IMiddleware"/>
        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                UserProfile userProfile = await _accessors.UserProfileAccessor.GetAsync(turnContext, () => new UserProfile());

                ConversationData conversationData =
                    await _accessors.ConversationDataAccessor.GetAsync(turnContext, () => new ConversationData());

                // Handle Message activity type, which is the main activity type for shown within a conversational interface
                // Message activities may contain text, speech, interactive cards, and binary or unknown attachments.
                // see https://aka.ms/about-bot-activity-message to learn more about the message and other activity types
                if (turnContext.Activity.Type == ActivityTypes.Message)
                {
                    // Get the conversation state from the turn context.
                    //var state = await _accessors.CounterState.GetAsync(turnContext, () => new CounterState());

                    //// Bump the turn count for this conversation.
                    //state.TurnCount++;

                    //// Set the property using the accessor.
                    //await _accessors.CounterState.SetAsync(turnContext, state);

                    //// Save the new turn count into the conversation state.
                    //await _accessors.ConversationState.SaveChangesAsync(turnContext);

                    //// Echo back to the user whatever they typed.
                    //var responseMessage = $"Turn {state.TurnCount}: You sent '{turnContext.Activity.Text}'\n";
                    //await turnContext.SendActivityAsync(responseMessage);

                    if (string.IsNullOrEmpty(userProfile.Name))
                    {
                        if (conversationData.PromptedUserForName)
                        {
                            userProfile.Name = turnContext.Activity.Text?.Trim();
                            await turnContext.SendActivityAsync($"Thanks {userProfile.Name}. What services are you interested in?");
                            var reply = turnContext.Activity.CreateReply();
                            reply.SuggestedActions = new SuggestedActions()
                            {
                                Actions = new List<CardAction>()
                        {
                            new CardAction()
                            {
                                Title="Payments & Credit Cards",
                                Value="Payments and credit cards.",
                                Type = ActionTypes.ImBack
                            },
                            new CardAction()
                            {
                                Title="Loans",
                                Value="Tell me about loans.",
                                Type = ActionTypes.ImBack
                            },
                            new CardAction()
                            {
                                Title="Insurance",
                                Value="What are the different insurance options?",
                                Type=ActionTypes.ImBack
                            }
                        }
                            };
                            await turnContext.SendActivityAsync(reply);
                            conversationData.PromptedUserForName = false;
                        }
                    }
                    else
                    {
                        var dialogContext = await _dialogs.CreateContextAsync(turnContext, cancellationToken);
                        var results = await dialogContext.ContinueDialogAsync(cancellationToken);

                        if (!conversationData.IsDialogActive)
                        {
                            var recognizerResult = await _services.LuisServices["ABNAMRO"].RecognizeAsync(turnContext, cancellationToken);
                            var topIntent = recognizerResult?.GetTopScoringIntent();
                            if (topIntent != null && topIntent.HasValue && topIntent.Value.intent != "None")
                            {
                                await turnContext.SendActivityAsync($"==>LUIS Top Scoring Intent: {topIntent.Value.intent}, Score: {topIntent.Value.score}\n");
                                // If the DialogTurnStatus is Empty we should start a new dialog.
                                if (results.Status == DialogTurnStatus.Empty)
                                {
                                    
                                    await dialogContext.BeginDialogAsync("details", null, cancellationToken);
                                    conversationData.IsDialogActive = true;
                                }
                            }
                            else
                            {
                                var reply = turnContext.Activity.CreateReply();
                                reply.Text = "Sorry. I couldn't understand that!";
                                await turnContext.SendActivityAsync(reply);
                            }
                        }
                    }
                }
                else if (turnContext.Activity.Type == ActivityTypes.ConversationUpdate)
                {
                    foreach(var item in turnContext.Activity.MembersAdded)
                    {
                        if (item.Id == turnContext.Activity.Recipient.Id){
                            var reply = turnContext.Activity.CreateReply();

                            var card = new HeroCard()
                            {
                                Images = new List<CardImage>()
                        {
                            new CardImage(url: @"C:\Users\harsh.soni\source\repos\ABNAMRO.DigitalAssistantSol\ABNAMRODigitalAssistant\img\WelcomeLogo.png")
                        },
                                Title = "Hello there! Let's get started.",
                                Subtitle = "I am a digital assistant that can guide you with our products and services.",
                            };

                            reply.Attachments.Add(card.ToAttachment());
                            await turnContext.SendActivityAsync(reply);
                            await turnContext.SendActivityAsync("What is your name?");
                            conversationData.PromptedUserForName = true;
                        }
                    }
                }
                else
                {
                    await turnContext.SendActivityAsync($"{turnContext.Activity.Type} event detected");
                }

                await _accessors.UserProfileAccessor.SetAsync(turnContext, userProfile);
                await _accessors.ConversationDataAccessor.SetAsync(turnContext, conversationData);
                await _accessors.UserState.SaveChangesAsync(turnContext);
                await _accessors.ConversationState.SaveChangesAsync(turnContext);
     
            }
            catch(Exception ex)
            {
                await turnContext.SendActivityAsync(ex.Message);
            }

        }

        private static async Task<DialogTurnResult> TypeStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var choices = new List<Choice>();
            foreach(var item in Enum.GetValues(typeof(BankAccountType))){
                choices.Add(new Choice { Value = item.ToString() });
            }
            return await stepContext.PromptAsync("choice", new PromptOptions { Choices = choices, Prompt = MessageFactory.Text("Which type of account would that be?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> PostalCodeStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            // Update the profile.
            userProfile.Account = new BankAccount();
            dynamic temp = stepContext.Result.GetType().GetProperty("Value").GetValue(stepContext.Result);
            userProfile.Account.Type = (BankAccountType)Enum.Parse(typeof(BankAccountType), temp, true);

            return await stepContext.PromptAsync("text", new PromptOptions { Prompt = MessageFactory.Text("We will need your address for creating your account. Please provide your postal code") }, cancellationToken);
        }


        private async Task<DialogTurnResult> PostalCodeConfirmStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Get the current profile object from user state.
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            // Update the profile.
            userProfile.Account.Address = new Dictionary<string, string>();
            userProfile.Account.Address["PostalCode"] = stepContext.Result.ToString();

            // We can send messages to the user at any point in the WaterfallStep.
            if (string.IsNullOrEmpty((string)stepContext.Result))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Invalid Postal Code."), cancellationToken);
            }
            else
            {
                // We can send messages to the user at any point in the WaterfallStep.
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I have your Postal Code as {userProfile.Account.Address["PostalCode"]}."), cancellationToken);
            }

            // WaterfallStep always finishes with the end of the Waterfall or with another dialog, here it is a Prompt Dialog.
            return await stepContext.PromptAsync("confirm", new PromptOptions { Prompt = MessageFactory.Text("Is this ok?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> HouseNumberStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            if ((bool)stepContext.Result)
            {
                return await stepContext.PromptAsync("text", new PromptOptions { Prompt = MessageFactory.Text("Please provide your house number: ") }, cancellationToken);
            }
            else
            {
                return await stepContext.NextAsync(string.Empty, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> HouseNumberConfirmStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Get the current profile object from user state.
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            // Update the profile.
            //userProfile.Account.Address = new Dictionary<string, string>();
            userProfile.Account.Address["HouseNumber"] = stepContext.Result.ToString();

            // We can send messages to the user at any point in the WaterfallStep.
            if (string.IsNullOrEmpty((string)stepContext.Result))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"No house number given."), cancellationToken);
            }
            else
            {
                // We can send messages to the user at any point in the WaterfallStep.
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I have your House Number as {userProfile.Account.Address["HouseNumber"]}."), cancellationToken);
            }

            // WaterfallStep always finishes with the end of the Waterfall or with another dialog, here it is a Prompt Dialog.
            return await stepContext.PromptAsync("confirm", new PromptOptions { Prompt = MessageFactory.Text("Is this ok?") }, cancellationToken);
        }


        private async Task<DialogTurnResult> StreetNameStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            if ((bool)stepContext.Result)
            {
                return await stepContext.PromptAsync("text", new PromptOptions { Prompt = MessageFactory.Text("Which street is your house in?") }, cancellationToken);
            }
            else
            {
                return await stepContext.NextAsync(-1, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> StreetNameConfirmStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Get the current profile object from user state.
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            // Update the profile.
           // userProfile.Account.Address = new Dictionary<string, string>();
            userProfile.Account.Address["StreetName"] = stepContext.Result.ToString();

            // We can send messages to the user at any point in the WaterfallStep.
            if (string.IsNullOrEmpty((string)stepContext.Result))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Couldn't get that street name."), cancellationToken);
            }
            else
            {
                // We can send messages to the user at any point in the WaterfallStep.
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I have your Street name as {userProfile.Account.Address["StreetName"]}."), cancellationToken);
            }

            // WaterfallStep always finishes with the end of the Waterfall or with another dialog, here it is a Prompt Dialog.
            return await stepContext.PromptAsync("confirm", new PromptOptions { Prompt = MessageFactory.Text("Is this ok?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> DetailsSummaryConfirmStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

            if ((bool)stepContext.Result)
            {
                string completeAddress = userProfile.Account.Address["HouseNumber"] + ", " + userProfile.Account.Address["StreetName"] + ", " + userProfile.Account.Address["PostalCode"];

                await stepContext.Context.SendActivityAsync(MessageFactory.Text(
                    $"I have your details as follows: " + completeAddress 
                    ), cancellationToken);
            }
            else
            {
                // We can send messages to the user at any point in the WaterfallStep.
                
            }
            return await stepContext.PromptAsync("confirm", new PromptOptions { Prompt = MessageFactory.Text("Is this ok?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> SummaryAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);
            if ((bool)stepContext.Result)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text(
                    $"Your account is successfully created. Your debit card details have been mailed to your registered mail."
                    ), cancellationToken);
            }
            else
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thanks. Your account will not be kept."), cancellationToken);
            }
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

        //    private async Task<DialogTurnResult> ShowDebitCardAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        //{
        //    var userProfile = await _accessors.UserProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);

        //    if ((bool)stepContext.Result)
        //    {
        //        // Get the current profile object from user state.
        //        await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I have your name as {userProfile.Name}."), cancellationToken);
        //    }
        //    else
        //    {
        //        // We can send messages to the user at any point in the WaterfallStep.
        //        await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thanks. Your account will not be kept."), cancellationToken);
        //    }

        //    // WaterfallStep always finishes with the end of the Waterfall or with another dialog, here it is the end.
        //    return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        //}







    }
}
